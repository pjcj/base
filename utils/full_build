#!/usr/bin/env zsh

# set -x

gobase() { cd ~/g/base || exit 1; }

gobase

source ~/.zplug/init.zsh
zplug clear
curl -SL https://github.com/docker/cli/raw/master/contrib/completion/zsh/_docker \
    >| ~/g/base/zsh/functions/_docker
curl -SL https://raw.githubusercontent.com/docker/compose/master/contrib/completion/zsh/_docker-compose \
    >| ~/g/base/zsh/functions/_docker-compose
curl -SL https://raw.githubusercontent.com/docker/machine/master/contrib/completion/zsh/_docker-machine \
    >| ~/g/base/zsh/functions/_docker-machine
curl -SL https://raw.githubusercontent.com/perlpunk/shell-completions/master/zsh/_dzil \
    >| ~/g/base/zsh/functions/_dzil
rm -f ~/.zcompdump; autoload -Uz compinit && compinit -i

if [ -d ~/.tmux/plugins/tpm ]; then
    ~/.tmux/plugins/tpm/bindings/clean_plugins
    ~/.tmux/plugins/tpm/bindings/install_plugins
    ~/.tmux/plugins/tpm/scripts/update_plugin_prompt_handler.sh all
fi

if [ "$ISOSX" ]; then
    brew update
    brew upgrade
    pip3=pip
else
    pip3=pip3
    sudo aptitude -y install golang python-pip
fi

$pip3 install --upgrade pip

$pip3 install --upgrade --user neovim
$pip3 install --upgrade --user vim-vint proselint
pip install --upgrade --user git+git://github.com/powerline/powerline  # python2

# sudo gem install colorls
# sudo gem update colorls

# urlwatch
$pip3 install --upgrade --user pyyaml minidb requests keyring chump urlwatch

# denite
$pip3 install --upgrade --user typing

$pip3 install --upgrade --user thefuck

if [ "$EDITOR" = "nvim" ]; then
    $EDITOR -c PlugUpgrade -c qa
    # $EDITOR -c PlugClean   -c qa
    $EDITOR -c PlugInstall -c qa
    $EDITOR -c PlugUpdate  -c qa

    $EDITOR -c UnicodeDownload -c qa
fi

gobase

command go get -u github.com/motemen/ghq

build_all
